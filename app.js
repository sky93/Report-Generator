var DocxGen = require('docxtemplater');

var input = {
    "person_name": "اصغر",
    "person_lastname": "اصغری",
    "person_father_rname": "جعفر",
    "person_birth": "1/1/1",
    "person_birth_place": "مشهد",
    "personel_id": "12345678",
    "person_add_city": "اصغر آباد",
    "person_add_main_street": "شهید اصغری",
    "person_add_sub_street": "کوچه اصغر",
    "person_add_pelak": "7",
    "person_p_tetel": "057",
    "person_tel": "81818181",
    "person_add_code_posti": "12121212",
    "person_mob": "09436868",
    "person_email": "support@asghar.com",
    "person_website": "asghar.ir",
    "person_maghta": "کلاس اول ۲",
    "person_takhsos": "پیکان جوانان",
    "person_uni_kind": "دانشگاه صنعتی سجاد",
    "nezam_pezeshki_num": "اصغر عدد",
    "person_public_work": "مکانیکی اصغر و پسران",
    "markaz_city": "مشهد",
    "markaz_parvane_s_date": "1/1/1",
    "markaz_parvane_date": "1/1/2",
    "markaz_kind_name": "بیمارستان",
    "markaz_name": "اصغر و پسران",
    "title": "اقای"
};

new DocxGen().
    loadFromFile("tagExample.docx", {async: true})
    .success(function (doc) {
        doc.setTags(input);
        doc.applyTags();
        doc.output();
    });

